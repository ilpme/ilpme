# Informations Libres Pour Mon Entreprise

J'ai trouvé assez peu d'exemples quand j'ai eu besoin de monter un dossier Crédit Impôt Innovation, je me suis dit que je publierai mon dossier pour aider les suivants.
Le but de ce projet est de regrouper des exemples de dossiers de demande pour le Crédit Impôt Innovation et le Crédit Impôt Recherche, ou toute autre démarche administrative utile aux entreprises. 

## Le CII/CIR, qu'est ce que c'est?
En tant qu'entreprise, vous pouvez monter un dossier pour être agréé CII/CIR, ce vous permet de déduire de vos impôts une partie de vos dépenses dédiées à l'innovation, cela permet aussi à vos clients de déduire une partie de vos prestations de leurs impôts. 
Plus d'informations sur [le site du gouvernement](https://www.entreprises.gouv.fr/politique-et-enjeux/credit-impot-innovation)

## Suis-je un expert en CII?
Non pas du tout, je suis un juste un développeur freelance qui a eu besoin d'être agréé CII. J'avais un projet de produit qui ne s'est finalement pas fait mais qui m'a permis d'avoir mon agrément  CII.

## Organisation du projet
Les informations pour chaque projet sont stockées dans des répertoires nommés de la façon suivante : <année>-<nom du projet>

## Licence
Ce projet est sous licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr)
Vous avez le droit de vous inspirer de ce projet pour écrire un dossier CII/CIR mais vous n'avez pas le droit de revendre ces documents ou de les inclure dans des packs de documents vendus en ligne.
Concernant l'attribution, il n'est pas nécessaire de mentionner la licence et le projet d'origine lorsque vous envoyez les documents à l'administration.
